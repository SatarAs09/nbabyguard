<?php

class UserModel
{
    private $idUsers,
        $Nom,
        $Prenom,
        $Adresse,
        $CodePostal,
        $Email,
        $Telephone,
        $Pass,
        $Verification,
        $NbrEnfant,
        $Roles_idRoles;

    public function __construct()
    {
    }


    public static function valider()
    {
        if (isset($_POST['submit'])) {


            $bdd = new Model();


            $error = array();
            $success = false;


            $Nom = trim(strip_tags($_POST['Nom']));
            $Prenom = trim(strip_tags($_POST['Prenom']));
            $Adresse = trim(strip_tags($_POST['Adresse']));
            $CodePostal = trim(strip_tags($_POST['CodePostal']));
            $Email = trim(strip_tags($_POST['Email']));
            $Telephone = trim(strip_tags($_POST['Telephone']));
            $Pass = trim(strip_tags($_POST['Pass']));
            $NbrEnfant = trim(strip_tags($_POST['NbrEnfant']));

            $verif = random_bytes(5);
            $verif = bin2hex($verif);


            if (!empty($Email)) {
                $sql = "SELECT Email FROM users WHERE Email = :Email";
                $query = $bdd->preparation($sql);
                $query->bindValue(':Email', $Email, PDO::PARAM_STR);
                $query->execute();
                $loginEmail = $query->fetch();
                if (!empty($loginEmail)) {
                    $error['1'] = 'Cet email est déjà utilisé';
                }
            } else {
                $error['2'] = 'Veuillez entrer un email';
            }

            if (empty($Telephone)) {
                $error['3'] = 'Veuillez entrer un numéro de téléphone.';
            }

            if (!empty($Nom)) {
                if (strlen($Nom) < 3) {
                    $error['4'] = 'Votre nom doit comprendre au minium 3 caractères.';
                } elseif (strlen($Nom) > 50) {
                    $error['5'] = 'Votre nom doit comprendre au maximum 50 caractères.';
                }
            } else {
                $error['6'] = 'Veuillez indiquez votre nom';
            }


            if (!empty($Prenom)) {
                if (strlen($Prenom) < 3) {
                    $error['7'] = 'Votre prénom doit comprendre au minium 3 caractères.';
                } elseif (strlen($Prenom) > 50) {
                    $error['8'] = 'Votre prénom doit comprendre au maximum 50 caractères.';
                }
            } else {
                $error['9'] = 'Veuillez indiquez votre prénom';
            }


            if (!empty($Pass)) {
                if (strlen($Pass) < 6) {
                    $error['10'] = 'Le mot de passe est trop court. (Minimum 6 caractères)';
                } elseif (strlen($Pass) > 50) {
                    $error['11'] = 'Le mot de passe est trop long. (Maximum 50 caractères)';
                }
            } else {
                $error['12'] = 'Veuillez renseigner un mot de passe';
            }


            if (count($error) == 0) {

                $success = true;
                $pass_hache = password_hash($Pass, PASSWORD_BCRYPT);
                $sql = "INSERT INTO users (idUsers,Nom,Prenom,Adresse,CodePostal,Email,Telephone,Pass,NbrEnfant,Roles_idRoles) VALUES(NULL, :Nom,:Prenom, :Adresse, :CodePostal, :Email,:Telephone,:Pass,:NbrEnfant,3)";
                $query = $bdd->preparation($sql);
                $query->bindValue(':Nom', $Nom, PDO::PARAM_STR);
                $query->bindValue(':Prenom', $Prenom, PDO::PARAM_STR);
                $query->bindValue(':Adresse', $Adresse, PDO::PARAM_STR);
                $query->bindValue(':CodePostal', $CodePostal, PDO::PARAM_STR);
                $query->bindValue(':Email', $Email, PDO::PARAM_STR);
                $query->bindParam(':Telephone', $Telephone, PDO::PARAM_STR);
                $query->bindValue(':Pass', $pass_hache, PDO::PARAM_STR);
                $query->bindValue(':NbrEnfant', $NbrEnfant, PDO::PARAM_STR);
                $query->execute();
                ?>
                <SCRIPT LANGUAGE="JavaScript">
                    document.location.href="<?=PUB_PATH?>/Inscription/validation"
                </SCRIPT>;
                <?php
            } else {
                for ($i = 1; $i <= 13; $i++) {
                    if (!empty ($error[$i])) {
                        echo $error[$i] . '<br>';
                    }
                }
            }


            $cle = md5(microtime(TRUE) * 100000);

            $bdd = new Model();

            $stmt = $bdd->preparation("UPDATE users SET Verification=:Verification WHERE Email like :Email");
            $stmt->bindParam(':Verification', $cle);
            $stmt->bindParam(':Email', $Email);
            $stmt->execute();



            $destinataire = $_POST['Email'];
            $sujet = "Activer votre compte";
            $entete = "From: inscription@nursery.com";

            $message = 'Bienvenue sur Nbabyguard,

Pour activer votre compte, veuillez cliquer sur le lien ci dessous
ou copier/coller dans votre navigateur internet.


http://localhost/nursery/Users/activation.php?email=' . urlencode($Email) . '&cle=' . urlencode($cle) . '

---------------
Ceci est un mail automatique, Merci de ne pas y repondre.';


            mail($destinataire, $sujet, $message, $entete);


        }
    }

    public static function loginUser(){
        if(isset($_POST['submit'])){
            try{
                $bdd = new Model();

                $Email = $_POST['Email'];

                $req = $bdd->preparation("SELECT idUsers, Pass FROM users WHERE Email = :Email ");
                $req->execute(array(
                    ':Email' => $Email));
                $resultat = $req->fetch();

                $validePass = password_verify($_POST['Pass'], $resultat['Pass']);
                if(empty($resultat)){

                    echo 'Erreur dans la saisie de vos identifiants';

                }else{
                    if($validePass){
                        $req= $bdd->requete("SELECT * FROM users WHERE Email ='$Email'")->fetchAll(PDO::FETCH_ASSOC);
                        $_SESSION['IDUSER']=$req[0]['idUsers'];
                        if (isset($_POST['submit'])) {
                            echo '<script>document.location.href="http://localhost/nbabyguard/Profil/parent"</script>';
                        }
                    } else{
                        echo 'Erreur dans la saisie de vos identifiants';
                    }
                }
            }
            catch (PDOException $e){
                $e->getMessage();
            }
        }
    }

    public static function logout()
    {
        session_destroy();
        $_SESSION['IDUSER'] = null;
        ?>
        <script>
            document.location.href="http://localhost/nbabyguard/";
        </script>
        <?php
    }

    public static function editProfil(){
        if(isset($_POST['Modifier'])){

            $Adresse    = $_POST['Adresse'];
            $CodePostal = $_POST['CodePostal'];
            $Email      = $_POST['Email'];
            $Telephone  = $_POST['Telephone'];
            $idUsers    = $_SESSION['idUsers'];

            try{
                $db = new Model();
                $sql = "UPDATE users SET Adresse='$Adresse', CodePostal='$CodePostal', Email='$Email', Telephone='$Telephone' WHERE idUsers='$idUsers'";
            }catch(PDOException $e){
                $e->getMessage();
            }
        }
    }

    public static function recuperation($arr){
        if (!empty($arr)){

            $db = new Model();

            $emailValeur   = $arr['Email'];
            $keyValeur     = $arr['key'];
            $keyUserExist  = $db->requete("SELECT Verification FROM users WHERE Email ='$emailValeur'")->fetch();


            if ($keyUserExist['Verification'] != false && $keyValeur != $keyUserExist['Verification']){
                echo "Erreur dans la saisie de votre clé de vérification";
            } else if ($keyUserExist['Verification'] == false) {
                echo "Identifiants ou clé de vérification invalide";
            }else {


                $random = random_bytes(9);
                $newPass = bin2hex($random);
                $hashPass = password_hash($newPass, PASSWORD_DEFAULT);

                $destinataire = $_POST['Email'];
                $sujet = "Réinitialisation de votre Mot de Passe";
                $entete = "From: inscription@nursery.com";

                $message = 'Vous avez souhaité réinitialiser votre Mot de Passe, voici la marche à suivre,

Pour réinitialiser votre Mot de passe, veuillez saisir la clé suivante :


'. urlencode($emailValeur) . '&cle=' . urlencode($keyValeur) . '

---------------
Ceci est un mail automatique, Merci de ne pas y repondre.';


                mail($destinataire, $sujet, $message, $entete);

                $db->requete("UPDATE users SET Pass =$hashPass WHERE Email = $emailValeur");
                echo 'Réinitialisation de votre Mot de Passe effectuée';

            }

        }

    }

    public static function index()
    {
        $model = new Model();
        $users = $model->select('users', "WHERE idUsers= 1");
        $users = $users->fetch();

        return $users;
    }

}
