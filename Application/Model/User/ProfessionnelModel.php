<?php

class ProfessionnelModel
{

    private $idProfessionnels,
            $Nom,
            $Prenom,
            $Adresse,
            $CodePostal,
            $Email,
            $Telephone,
            $Pass,
            $Societe,
            $NumeroAgrement,
            $NomCreche,
            $photo,
            $CoutHoraire,
            $MinAge,
            $MaxAge;
        //    $Verification;

    public function __construct()
    {
    }

    public static function submit(){
        if(isset($_POST['submit'])){
            $bdd = new Model();

            $error   = array();
            $success = false;

            $Nom            = trim(strip_tags($_POST['Nom']));
            $Prenom         = trim(strip_tags($_POST['Prenom']));
            $Adresse        = trim(strip_tags($_POST['Adresse']));
            $CodePostal     = trim(strip_tags($_POST['CodePostal']));
            $Email          = trim(strip_tags($_POST['Email']));
            $Telephone      = trim(strip_tags($_POST['Telephone']));
            $Pass           = trim(strip_tags($_POST['Pass']));
            $Societe        = trim(strip_tags($_POST['Societe']));
            $NumeroAgrement = trim(strip_tags($_POST['NumeroAgrement']));
            $NomCreche      = trim(strip_tags($_POST['NomCreche']));
            $photo          = trim(strip_tags($_POST['photo']));
            $CoutHoraire    = trim(strip_tags($_POST['CoutHoraire']));
            $MinAge         = trim(strip_tags($_POST['MinAge']));
            $MaxAge         = trim(strip_tags($_POST['MaxAge']));
         //   $Verification   = trim(strip_tags($_POST['Verification']));

            $verif = random_bytes(5);
            $verif = bin2hex($verif);

            if (!empty($Email)) {
                $sql = "SELECT Email FROM professionnels WHERE Email = :Email";
                $query = $bdd->preparation($sql);
                $query->bindValue(':Email', $Email, PDO::PARAM_STR);
                $query->execute();
                $loginEmail = $query->fetch();
                if (!empty($loginEmail)) {
                    $error['1'] = 'Cet email est déjà utilisé';
                }
            } else {
                $error['2'] = 'Veuillez entrer un email';
            }

            if (empty($Telephone)) {
                $error['3'] = 'Veuillez entrer un numéro de téléphone.';
            }

            if (!empty($Nom)) {
                if (strlen($Nom) < 3) {
                    $error['4'] = 'Votre nom doit comprendre au minium 3 caractères.';
                } elseif (strlen($Nom) > 50) {
                    $error['5'] = 'Votre nom doit comprendre au maximum 50 caractères.';
                }
            } else {
                $error['6'] = 'Veuillez indiquez votre nom';
            }


            if (!empty($Prenom)) {
                if (strlen($Prenom) < 3) {
                    $error['7'] = 'Votre prénom doit comprendre au minium 3 caractères.';
                } elseif (strlen($Prenom) > 50) {
                    $error['8'] = 'Votre prénom doit comprendre au maximum 50 caractères.';
                }
            } else {
                $error['9'] = 'Veuillez indiquez votre prénom';
            }


            if (!empty($Pass)) {
                if (strlen($Pass) < 6) {
                    $error['10'] = 'Le mot de passe est trop court. (Minimum 6 caractères)';
                } elseif (strlen($Pass) > 50) {
                    $error['11'] = 'Le mot de passe est trop long. (Maximum 50 caractères)';
                }
            } else {
                $error['12'] = 'Veuillez renseigner un mot de passe';
            }

            if (count($error) == 0) {

                $success = true;
                $pass_hache = password_hash($Pass, PASSWORD_BCRYPT);
                $sql = "INSERT INTO professionnels (idProfessionnels,Nom,Prenom,Adresse,CodePostal,Email,Telephone,Pass,Societe,NumeroAgrement,NomCreche,photo,CoutHoraire,MinAge,MaxAge,Enfant_idEnfant) VALUES(NULL,:Nom,:Prenom, :Adresse, :CodePostal, :Email,:Telephone,:Pass,:Societe, :NumeroAgrement, :NomCreche, :photo, :CoutHoraire, :MinAge, :MaxAge,1)";
                $query = $bdd->preparation($sql);
                $query->bindValue(':Nom', $Nom, PDO::PARAM_STR);
                $query->bindValue(':Prenom', $Prenom, PDO::PARAM_STR);
                $query->bindValue(':Adresse', $Adresse, PDO::PARAM_STR);
                $query->bindValue(':CodePostal', $CodePostal, PDO::PARAM_STR);
                $query->bindValue(':Email', $Email, PDO::PARAM_STR);
                $query->bindParam(':Telephone', $Telephone, PDO::PARAM_STR);
                $query->bindValue(':Pass', $pass_hache, PDO::PARAM_STR);
                $query->bindValue(':Societe', $Societe, PDO::PARAM_STR);
                $query->bindValue(':NumeroAgrement', $NumeroAgrement, PDO::PARAM_STR);
                $query->bindValue(':NomCreche', $NomCreche, PDO::PARAM_STR);
                $query->bindValue(':photo', $photo, PDO::PARAM_STR);
                $query->bindValue(':CoutHoraire', $CoutHoraire, PDO::PARAM_STR);
                $query->bindValue(':MinAge', $MinAge, PDO::PARAM_STR);
                $query->bindValue(':MaxAge', $MaxAge, PDO::PARAM_STR);
              //  $query->bindValue(':Verification', $Verification, PDO::PARAM_STR);
                $query->execute();

                ?>
                <SCRIPT LANGUAGE="JavaScript">
                document.location.href="<?=PUB_PATH?>/Inscription/validation"
                    </SCRIPT>;
                <?php
            } else {
                for ($i = 1; $i <= 13; $i++) {
                    if (!empty ($error[$i])) {
                        echo $error[$i] . '<br>';
                    }
                }
            }

            $cle = md5(microtime(TRUE) * 100000);





            $destinataire = $_POST['Email'];
            $sujet = "Activer votre compte";
            $entete = "From: inscription@nursery.com";

            $message = 'Bienvenue sur Nbabyguard,

Pour activer votre compte, veuillez cliquer sur le lien ci dessous
ou copier/coller dans votre navigateur internet.


'. urlencode($Email) . '&cle=' . urlencode($cle) . '

---------------
Ceci est un mail automatique, Merci de ne pas y repondre.';


            mail($destinataire, $sujet, $message, $entete);


        }
    }

    public static function loginPro(){
        if(isset($_POST['submit'])){
            try{
                $bdd = new Model();



                $Email = $_POST['Email'];

                $req = $bdd->preparation("SELECT idProfessionnels, Pass FROM professionnels WHERE Email = :Email ");
                $req->execute(array(
                    ':Email' => $Email));
                $resultat = $req->fetch();

                $validePass = password_verify($_POST['Pass'], $resultat['Pass']);
                if(empty($resultat)){

                    echo 'Erreur dans la saisie de vos identifiants';

                }else{
                    if($validePass){
                        //session_start();
                     //   $_SESSION['idProfessionnels'] = $resultat['idProfessionnels'];
                      //  $_SESSION['Email']   = $Email;

                        $req= $bdd->requete("SELECT * FROM professionnels WHERE Email ='$Email'")->fetchAll(PDO::FETCH_ASSOC);
                        $_SESSION['IDPRO']=$req[0]['idProfessionnels'];
                        if (isset($_POST['submit'])) {
                            echo '<script>document.location.href="http://localhost/nbabyguard/Profil/pro"</script>';
                        }
                    } else{
                        echo 'Erreur dans la saisie de vos identifiants';
                    }
                }
            }
            catch (PDOException $e){
                $e->getMessage();
            }
        }
    }

    public static function index(){
        $model = new Model();
        $pros = $model->select('professionnels',  " WHERE idProfessionnels=".$_SESSION['IDPRO']);
        $pros = $pros->fetch();

        return $pros;
    }

}
