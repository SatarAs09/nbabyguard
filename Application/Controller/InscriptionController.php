<?php

class InscriptionController
{
    // Pro
    public function pro()
    {
        include VIEW_PATH .'Form/FormPro.php';
        include 'Application/Model/User/ProfessionnelModel.php';
        if(isset($_POST['submit'])){
            ProfessionnelModel::submit();
        }
    }

    // Parent
    public function parent()
    {
        include VIEW_PATH . 'Form/FormUsers.php';
        include 'Application/Model/User/UserModel.php';
        if (isset($_POST['submit'])) {
            UserModel::valider();
        }
    }

    public function validation()
    {
        include VIEW_PATH .'Form/FormValidation.php';
        if (isset($_POST['submit'])) {
           echo '<script>document.location.href="http://localhost/nbabyguard/SelectionConnexion/index"</script>';
        }
    }

    /*public function reinitialisation(){
        include VIEW_PATH . 'Form/FormRecup.php';
        include 'Application/Model/User/UserModel.php';
        if(isset($_POST['Valider'])){
            UserModel::recuperation(9);
        }
    } */
}