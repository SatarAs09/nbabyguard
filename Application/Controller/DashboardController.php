<?php

class DashboardController extends Framework_win
{
    public function pro()
    {
        include './Application/Model/User/ProfessionnelModel.php';
        $pros = ProfessionnelModel::index();
        $this->render("espacePro",[
            'pros'=>$pros
        ]);
    }

    public function parent(){
        include './Application/Model/User/UserModel.php';
        $users = UserModel::index();
        $this->render("espaceUser",[
            'users'=>$users
        ]);
    }
}