<?php

class ConnexionController
{
    public function pro(){
        include VIEW_PATH .'Form/Connexion.php';
        include 'Application/Model/User/ProfessionnelModel.php';
        if(isset($_POST['submit'])){

            ProfessionnelModel::loginPro();
        }
    }

    public function parent(){

        include VIEW_PATH . 'Form/Connexion.php';
        include  'Application/Model/User/UserModel.php';
        if(isset($_POST['submit'])) {

            UserModel::loginUser();
        }
    }
}

