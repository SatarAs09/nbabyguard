<?php

class ProfilController extends Framework_win
{
    public function pro(){
        include './Application/Model/User/ProfessionnelModel.php';
        $pros = ProfessionnelModel::index();
        $this->render("ProfilPro",[
            'pros'=>$pros
        ]);
    }

    public function parent(){
        include './Application/Model/User/UserModel.php';
        $users = UserModel::index();
        $this->render("ProfilUser",[
            'users'=>$users
        ]);
        if(isset($_POST['update'])){
            UserModel::editProfil();
        }

    }
}