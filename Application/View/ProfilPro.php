<?php
?>

<main>

    <div class="  container-fluid">
        <div id="disqus_thread"></div>
        <script>

            /**
             *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
             *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
            /*
            var disqus_config = function () {
            this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
            this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
            };
            */
            (function() { // DON'T EDIT BELOW THIS LINE
                var d = document, s = d.createElement('script');
                s.src = 'https://localhost-syeufmap35.disqus.com/embed.js';
                s.setAttribute('data-timestamp', +new Date());
                (d.head || d.body).appendChild(s);
            })();
        </script>
        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

    </div>
    <div class="col-lg-12">
        <img src="<?= $pros['photo']?>">
    </div>
    <div class="col-lg-12 text-center">
        <li>  Nom et Prenom : <?=  $pros['Nom']. " " .$pros['Prenom']?> </li>
        <li> Email : <?=  $pros['Email']?></li>
        <li> adresse :<?=  $pros['Adresse']. " ".$pros['CodePostal']?></li>
        <li>Nom creche :<a href="#"><?=  $pros ['NomCreche']?></a></li>
        <li>Nom societe : <?=$pros['Societe']?></li>
        <li> N° agrement :<?=  $pros['NumeroAgrement']?></li>
        <li> age d'accueil : <?= $pros['MinAge']."-".$pros['MaxAge']." ans"?></li>
        <li>Coût horaire : <?= $pros['CoutHoraire']." ". "€" ?></li>
        <li> Telephone :<?=  $pros['Telephone']?></li>
    </div>
</main>
