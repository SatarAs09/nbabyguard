<main>
    <div class="container spacer">
        <div class="card">
            <h5 class="card-header info-color white-text text-center py-4">
                <strong>Inscription des Professionnels</strong>
            </h5>
            <div class="card-body px-lg-5 pt-0">
                <form class="text-center" method="post" action="" style="color: #757575;">
                    <div class="form-row">
                        <div class="col">
                            <!-- Nom & Prenom -->
                            <div class="md-form">
                                <input type="text" id="Nom" name="Nom" class="form-control">
                                <label for="Nom">Nom</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="md-form">
                                <input type="text" id="Prenom" name="Prenom" class="form-control">
                                <label for="Prenom">Prenom</label>
                            </div>
                        </div>
                    </div>

                    <!-- Adresse & Code Postal -->
                    <div class="form-row">
                        <div class="col">
                            <div class="md-form mt-0">
                                <input type="text" id="Adresse" name="Adresse" class="form-control">
                                <label for="Adresse">Adresse</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="md-form mt-0">
                                <input type="text" id="CodePostal" name="CodePostal" class="form-control">
                                <label for="CodePostal">Code Postal</label>
                            </div>
                        </div>
                    </div>

                    <!-- Email -->
                    <div class="md-form mt-0">
                        <input type="email" id="Email" name="Email" class="form-control">
                        <label for="Email">Email</label>
                    </div>

                    <!-- Telephone -->
                    <div class="md-form">
                        <input type="tel" id="Telephone" name="Telephone" class="form-control" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$">
                        <label for="Telephone">Numéro de Telephone</label>
                    </div>

                    <!-- Mot de passe -->
                    <div class="md-form">
                        <input type="password" id="Pass" name="Pass" class="form-control" aria-describedby="materialRegisterFormPasswordHelpBlock">
                        <label for="Pass">Mot de passe</label>
                        <small id="materialRegisterFormPasswordHelpBlock" class="form-text text-muted mb-4">
                            Un minimum de 6 caractères est requis
                        </small>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <div class="md-form mt-0">
                                <input type="text" id="Societe" name="Societe" class="form-control">
                                <label for="Societe">Societe</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="md-form mt-0">
                                <input type="text" id="NumeroAgrement" name="NumeroAgrement" class="form-control">
                                <label for="NumeroAgrement">Numero Agrementation</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <div class="md-form">
                                <input type="text" id="NomCreche" name="NomCreche" class="form-control">
                                <label for="NomCreche">Nom de crèche</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="md-form">
                                <label for="photo">Photo de la crèche</label>
                                <input type="file" id="photo" name="photo" class="dropify" data-default-file="url_of_your_file">
                            </div>
                        </div>
                    </div>
                    <div class="md-form">
                        <input type="text" id="CoutHoraire" name="CoutHoraire" class="form-control">
                        <label for="CoutHoraire">Coût Horaire</label>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <div class="md-form">
                                <input type="text" id="MinAge" name="MinAge" class="form-control">
                                <label for="MinAge">Age minimum</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="md-form">
                                <input type="text" id="MaxAge" name="MaxAge" class="form-control">
                                <label for="MaxAge">Age maximum</label>
                            </div>
                        </div>
                    </div>

                    <button class="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="reset" name="reset">Effacer</button>
                    <button class="btn btn-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit" name="submit">S'inscrire</button>

                    <hr>

                    <p>En cliquant sur
                        <em>s'inscrire</em> vous acceptez les
                        <a href="<?= PUB_PATH ;?>/Reglementation/cgu" target="_blank">Conditions générales d'utilisation</a> et le
                        <a href="<?= PUB_PATH ;?>/Reglementation/rgpd" target="_blank">Règlement général sur la protection des données</a>. </p>
                </form>
            </div>
        </div>
    </div>
</main>