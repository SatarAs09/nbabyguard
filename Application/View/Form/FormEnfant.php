<main>
    <div class="container spacer">
        <div class="card">
            <h5 class="card-header info-color white-text text-center py-4">
                <strong>Inscription des enfants</strong>
            </h5>
            <div class="card-body px-lg-5 pt-0">
                <form class="text-center" method="post" action="" style="color: #757575;">
                    <div class="form-row">
                        <div class="col">
                            <!-- Nom & Prenom -->
                            <div class="md-form">
                                <input type="text" id="Nom" name="Nom" class="form-control">
                                <label for="Nom">Nom</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="md-form">
                                <input type="text" id="Prenom" name="Prenom" class="form-control">
                                <label for="Prenom">Prenom</label>
                            </div>
                        </div>
                    </div>

                    <!-- Age -->
                    <div class="md-form mt-0">
                        <input type="date" id="Age" name="Age" class="form-control">
                        <label for="Age">Age</label>
                    </div>

                    <!-- Infos Complémentaires -->
                    <div class="md-form mt-0">
                        <input type="text" id="InfoComplementaire" name="InfoComplementaire" class="form-control">
                        <label for="InfoComplementaire">Infos Complémentaires</label>
                    </div>

                    <button class="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="reset" name="reset">Effacer</button>
                    <button class="btn btn-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit" name="valider">S'inscrire</button>
                </form>
            </div>
        </div>
    </div>
</main>



