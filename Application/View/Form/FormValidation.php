<main>
    <div class="container spacer">
        <div class="card">
            <h5 class="card-header info-color white-text text-center py-4">
                <strong>Validation de l'inscription</strong>
            </h5>
            <div class="card-body px-lg-5 pt-0">
                <form class="text-center" method="post" action="">
                    <p class="text-center w-responsive mx-auto mb-5">Veuillez saisir votre adresse mail ainsi que la clé qui vous a été envoyé,
                        afin de valider votre compte et profiter de la plateforme sans restriction.</p>

                    <div class="md-form">
                        <input type="email" id="Email" name="Email" class="form-control">
                        <label for="Email">Email</label>
                    </div>
                    <div class="md-form">
                        <input type="text" id="verif" name="verif" class="form-control">
                        <label for="verif">Clé de validation</label>
                    </div>

                    <button class="btn btn-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit" name="submit" value="submit">Valider mon inscription</button>

                </form>
            </div>
        </div>
    </div>
</main>