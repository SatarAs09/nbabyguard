<main>

<div class="main-content">
<div class="formulaire">
    <form class="register col s12" action="" method="post">

        <h3 class="my_title">Récupération de votre mot de passe</h3>
        <div class="input-field">
            <input id="icon_prefix" type="email" class="validate" name="Email" value="" placeholder="Email">
            <i class="material-icons prefix">add</i>
        </div>
        <div class="center-align">
            <button class="btn waves-effect waves-light" type="submit" name="Valider">Envoyer le mail
                <i class="material-icons right">send</i>
            </button>
        </div>
    </form>
</div>
</div>

</main>
