<main>
    <div class="container spacer">
        <div class="card">
            <h5 class="card-header info-color white-text text-center py-4">
                <strong>Connexion</strong>
            </h5>
            <div class="card-body px-lg-5 pt-0">
                <form class="text-center" method="post" action="#">

                    <div class="md-form">
                        <input type="email" id="Email" name="Email" class="form-control">
                        <label for="Email">Email</label>
                    </div>
                    <div class="md-form">
                        <input type="password" id="Pass" name="Pass" class="form-control">
                        <label for="Pass">Mot de passe</label>
                    </div>

                    <button class="btn btn-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit" name="submit">Connexion</button>
                </form>
            </div>
        </div>
    </div>
</main>


