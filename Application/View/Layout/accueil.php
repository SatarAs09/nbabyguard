<div id="parallax-world-of-ugg">

    <section>
        <div class="top-parallax parallax-one">
            <h2>Une solution efficace pour trouver une garderie!</h2>
        </div>
    </section>

    <!-- SECTION FEATURES -->
    <section class="section-features js--section-features" id="features">
        <div class="row">
            <h2>Trouver une garderie? Un jeu d'enfant!</h2>
            <p class="presentation">
                N'Baby Guard est la plateforme qui reconcilie travail, et rôle parental. Travailler et avoir des enfants peut-être un veritable parcours du combattant. Nous proposons une solution simple et efficace pour trouver une place en garderie ou crèche pour enfants de - de 6 ans grâce à nos fonctionnalités.
            </p>
        </div>

        <div class="row js--wp-1">

            <div class="icons-wrapper col span-1-of-3 box">
                <i class="icons1 far fa-eye"></i>
                <h3>Un oeil sur ses enfants</h3>
                <p>
                    Un tableau de bord est mis à votre disposition afin de pouvoir échanger directement avec les encadrants des crèches selectionnées selon vos critères. Vous êtes tenu informer de toutes les activités faites par vos enfants.
                </p>
            </div>
            <div class="icons-wrapper col span-1-of-3 box">
                <i class="icons2 far fa-clock"></i>
                <h3>Rapide et simple</h3>
                <p>
                    Parcourez la liste des professionels aux alentours. Discutez rapidement et posez vos questions directement aux encadrants grâce à notre système de chat.
                </p>
            </div>
            <div class="icons-wrapper col span-1-of-3 box">
                <i class="icons3 far fa-calendar-check"></i>
                <h3>Planning</h3>
                <p>
                    Grâce à nos calendrier, vous pourrez planifier directement chaque jour de la semaine. À quelle heure déposer ses enfants, à quelle heure les prendre, tout sera noté par l'équipe d'encadrants.
                </p>
            </div>

        </div>
    </section>
    <!-- .SECTION FEATURES-->

    <section>
        <div class="parallax-two">
            <h2>Pourquoi choisir N'Baby Guard?</h2>
        </div>
    </section>

    <section>
        <div class="block">
            <p><span class="first-character ny">U</span><p class="margin-top-10">n peu plus de la moitié (56 %) des 2,4 millions d’enfants de moins de trois ans dispose théoriquement d’une place dans l’un des différents modes de garde dits « formels » (crèche, assistante maternelle, école maternelle et emploi à domicile) selon l’Observatoire national de la petite enfance de la Cnaf (données 2015). Ce chiffre est théorique car pour être gardé toute la journée un enfant peut avoir besoin de deux places (école maternelle+assistante maternelle par exemple) ; la capacité réelle est donc inférieure.
                Près de 60 % des enfants de moins de trois ans accueillis dans l’un des modes de garde formels sont pris en charge par une assistante maternelle employée par un particulier, 7 % vont à l’école maternelle, 3 % bénéficient d’une personne employée à domicile et un petit tiers ont une place de crèche. Si l’on considère tous les enfants de 0 à 3 ans, un tiers est gardé par une assistante maternelle, 18 % disposent d’une place en crèche, 4 % sont scolarisés (un peu plus de 10 % des enfants de deux ans) et 2 % sont gardés à domicile par une personne salariée. Au total, pas moins de 44 % des jeunes enfants sont pris en charge par une mode « informel », autrement dit la « débrouille », par les parents (la mère le plus souvent), les grands-parents, la famille ou les amis.</p>
            <p class="line-break margin-top-10"></p>
            <p class="margin-top-10"><p class="margin-top-10">L’accueil de la petite enfance s’est détérioré entre le milieu des années 1990 et la fin des années 2000. Au cours de cette période, la fécondité a augmenté : 80 000 bébés de plus sont nés en 2006 qu’en 1994. Dans le même temps, le taux de scolarisation à deux ans en maternelle a baissé de 35 % à 12 % entre 2000 et 2011. La Cnaf note une amélioration dans les années récentes. Entre 2008 et 2015, la capacité théorique d’accueil a augmenté de 48,3 à 56,6 places pour 100 enfants. Cette hausse a résulté dans un premier temps de la croissance du nombre de places en crèches et surtout de l’augmentation des places d’assistantes maternelles. Depuis 2011, le taux de scolarisation à deux ans n’évolue guère. Plus récemment, l’amélioration de la capacité d’accueil des jeunes enfants résulte de la diminution de la fécondité enregistrée dans les années récentes : on a compté 50 000 naissances annuelles en moins entre 2010 et 2016.</p>
            <p class="line-break margin-top-10"></p>
            <p class="margin-top-10"><p class="margin-top-10">Cependant, d’ici 2022, 30.000 nouvelles places en crèche devraient être créées. Le Conseil d’administration de la Caisse nationale d’allocations familiales (Cnaf) a adopté une nouvelle convention qui va lier pendant cinq ans la branche famille de la sécurité sociale à l’Etat. C'est aussi pour cette raison que N'Baby Guard vous proposer de faciliter grâce à notre plateforme, votre recherche de place en crèche, vous mettant directement en contact avec les professionels du milieu. Vous pourrez ainsi discuter, faire le tri et choisir le ou les lieux le plus approprié pour vos enfants et correspondant le plus à vos critères.</p>
        </div>
    </section>

</div>

<div class="main-bottom">
    <img class="image-accueil" src="public/img/painted-hands-multiple1.jpg" alt="Garderie">
</div>
