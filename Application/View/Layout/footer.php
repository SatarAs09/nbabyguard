    <footer class="page-footer font-small pt-4">
        <div class="container text-center text-md-left">
            <div class="row text-center text-md-left mt-3">
                <div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
                    <h6 class="text-uppercase mb-4 font-weight-bold">N'Baby Guard</h6>
                    <p>Parce que l'épanouissement de vos enfants compte énormément pour nous,
                        N'Baby Guard, le service pour vous, proche de chez vous.</p>
                </div>
                <hr class="w-100 clearfix d-md-none">
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
                    <h6 class="text-uppercase mb-4 font-weight-bold">Fonctionnalités</h6>
                    <p>
                        <a href="#!">Listing Professionnels</a>
                    </p>
                    <p>
                        <a href="#!">Listing Parents</a>
                    </p>
                    <p>
                        <a href="#!">Listing crèches</a>
                    </p>
                </div>
                <hr class="w-100 clearfix d-md-none">
                <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">
                    <h6 class="text-uppercase mb-4 font-weight-bold">Liens utiles</h6>
                    <p>
                        <?php if (isset($_SESSION['IDUSER'])&& $_SESSION['IDUSER'] == 3){
                            ?>
                            <a href="<?php PUB_PATH ?>/ProfilParent/index" class="nav-link">Mon Profil</a>
                            <?php
                        } elseif (isset($_SESSION['IDPRO'])&& $_SESSION['IDPRO'] == 2) {
                            ?>
                            <a href="<?php PUB_PATH ?>/ProfilPro/index" class="nav-link">Mon Profil</a>
                            <?php
                        }
                        ?>
                    </p>
                    <p>
                        <a href="#!">Mon espace</a>
                    </p>
                    <p>
                        <a href="#!">Qui sommes-nous ?</a>
                    </p>
                    <p>
                        <a href="#!">Besoin d'aide ?</a>
                    </p>
                </div>
                <hr class="w-100 clearfix d-md-none">
                <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
                    <h6 class="text-uppercase mb-4 font-weight-bold">Nous Contacter</h6>
                    <p>
                        <i class="fa fa-home mr-3"></i>Place St Marc, 76000, Rouen</p>
                    <p>
                        <i class="fa fa-envelope mr-3"></i> nbabyguard@gmail.com</p>
                    <p>
                        <i class="fa fa-phone mr-3"></i>02 35 70 04 04</p>
                    <p>
                        <i class="fa fa-info-circle mr-3"></i>Du lundi au vendredi</p>
                    <p>
                        <i class="fas fa-clock mr-3"></i>De 9h00 à 17h30</p>
                </div>
            </div>
            <hr>
            <div class="row d-flex align-items-center">
                <div class="col-md-7 col-lg-8">
                    <p class="text-center text-md-left">© 2018 Copyright:
                        <a href="#">
                            <strong> N'Baby Guard</strong>
                        </a>
                        <a href="<?= PUB_PATH ;?>/Reglementation/cgu" class="cgu">
                            <strong>CGU</strong>
                        </a>
                        <a href="<?= PUB_PATH ;?>/Reglementation/rgpd" class="mentions">
                            <strong>Mentions légales</strong>
                        </a>
                        <a href="" class="faq">
                            <strong>FAQ</strong>
                        </a>
                    </p>
                </div>
                <div class="col-md-5 col-lg-4 ml-lg-0">
                    <div class="text-center text-md-right">
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item">
                                <a class="btn-floating btn-sm rgba-white-slight mx-1">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="btn-floating btn-sm rgba-white-slight mx-1">
                                    <i class="fab fa-facebook-square"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="btn-floating btn-sm rgba-white-slight mx-1">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="btn-floating btn-sm rgba-white-slight mx-1">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script> -->
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script> -->

    <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.14/js/mdb.min.js"></script>

    <!-- Font Awesome ( SVG & JS ) -->
    <script defer src="https://use.fontawesome.com/releases/v5.5.0/js/all.js" integrity="sha384-GqVMZRt5Gn7tB9D9q7ONtcp4gtHIUEW/yG7h98J7IpE3kpi+srfFyyB/04OV6pG0" crossorigin="anonymous"></script>

    <script id="dsq-count-scr" src="//localhost-syeufmap35.disqus.com/count.js" async></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>

    <?php

    ?>

    </body>
</html>