<main>
    <!--    <nav class="navbar navbar-expand-md navbar-dark mdb-color mb-5">
            <div class="mr-auto">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb d-inline-flex pl-0 pt-0">
                        <li class="breadcrumb-item"><a class="white-text" href="#!">Dashboard</a></li>
                        <li class="breadcrumb-item active">Espace Professionnel</li>
                    </ol>
                </nav>
            </div>
            <ul class="navbar-nav ml-auto nav-flex-icons">
                <li class="nav-item dropdown">
                    <a class="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">Voir mon profil &nbsp;<i class="fa fa-user"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link">1 <i class="fa fa-bell white-text"></i></a>
                </li>
            </ul>
        </nav>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-center">
                <li class="breadcrumb-item"><a href="#">Profil</a></li>
                <li class="breadcrumb-item"><a href="#">Modifier mes informations</a></li>
                <li class="breadcrumb-item"><a href="#">Liste des parents</a></li>
                <li class="breadcrumb-item dropdown"><a href="#">Ajouter une crèche</a></li>
            </ol>
        </nav>-->
    <?php
    /*$db = new Model();*/
    ?>

    <div class="profile-wrapper">
        <div class="profile-image">
            <img src="<?= PUB_PATH?>/public/img<?= DIRSEP;?>preschool2.jpg">
        </div>
        <div class="close"></div>
        <h2 class="profile-title"><?= $pros['Nom'].$pros['Prenom']?></h2>
        <div class="profile-info">
            <div class="content">
                <h3>Infos</h3>
                <li>Nom et Prenom : <?= $pros['Nom'].$pros['Prenom']?> </li>
                <li>Email : <?= $pros['Email']?></li>
                <li>Adresse :<?= $pros['Adresse']?></li>
                <li>Nombres d'enfants :<a href="#"><?= $pros['NbrEnfant']?></a></li>
                <li>Telephone :<?= $pros['Telephone']?></li>
                <p class="link one"><i class="fa fa-link" aria-hidden="true"></i><a href="http://example.com" target="_blank">www.example.com</a></p>
                <p class="link"><i class="fa fa-globe" aria-hidden="true"></i>France</p>
            </div>
            <div class="stats">
                <div class="stat">
                    <a>Profil</a>
                </div>
                <div class="stat">
                    <a>Modifier mes informations</a>
                </div>
                <div class="stat">
                    <a>Liste des parents</a>
                </div>
                <div class="stat">
                    <a>Ajouter une crèche</a>
                </div>
            </div>
            <div class="arrow one"></div>
            <div class="arrow two"></div>
        </div>
    </div>

    <!--    <iframe name="InlineFrame1" id="InlineFrame1" style="width:1000px;height:350px;"
                src="https://www.mathieuweb.fr/calendrier/calendrier-des-semaines.php?nb_mois=1&nb_mois_ligne=4&mois=0&an=0&langue=fr&texte_color=B9CBDD&week_color=DAE9F8&week_end_color=C7DAED&police_color=453413&sel=true"
                scrolling="no" frameborder="0" allowtransparency="true"></iframe>-->


    <?php
    echo '<br>';
    var_dump($pros);
    ?>
</main>
