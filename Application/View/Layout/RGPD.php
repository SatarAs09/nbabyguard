<?php
?>
<link rel="stylesheet" href="./style.css">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

<?php
?>
<main>
<div class="container-fluid" >
    <div class="row">
        <div class="rgpd_def col-lg-12">
            <h3>Le RGPD instaure de nouveaux droits pour les personnes dont les données personnelles sont traitées. Voici les principaux.</h3>
        </div>
        <hr>
    </div>
    <div class="row">
        <div class="rgpd_info col-lg-12">
            <h2>Informations :</h2>
            <p>Lorsqu'il collecte les données d'une personne, le responsable du traitement doit lui fournir un certain nombre d'informations.</p>
            <p>notamment l'identité et les coordonnées du responsable du traitement (et le cas échéant, les coordonnées du DPO), les finalités de ce traitement, ainsi
                que l'indication des destinataires de ces données.</p>

        </div>

        <hr>
    </div>
    <div class="row">
        <div class="rgpd_droiteff col-lg-12">
            <h2>Demande d'effacement et droit à l'oubli :</h2>
            <p>Le RGPD prévoit également un droit à l'effacement : </p>
            <p>la personne concernée peut demander l'effacement de ses données pour l'un des motifs listés dans l'article. </p>
            <p>Le responsable du traitement devra alors procéder à la suppression des données dans les meilleurs délais.</p>

        </div>
        <hr>
    </div>

    <div class="row">
        <div class="rgpd_droitport col-lg-12">
            <h2>Droit à la portabilité: </h2>
            <p>Le règlement crée un droit à la portabilité des données. Il permet, en quelque sorte, à une personne physique de s'approprier ses propres données et donc d'en demander </p>
            <p> - la restitution : la personne peut récupérer ses données afin de pouvoir les stocker et les réutiliser pour son usage personnel, comme bon lui semble ;</p>
              <p>  - le transfert à un autre responsable de traitement, l'un des objectifs affichés de ce nouveau droit consistant à faire jouer la concurrence entre les différents responsables de traitement (à l'image de ce qui existe en matière de portabilité des numéros de téléphone, par exemple).</p>

            <p>Le responsable de traitement d'origine ne pourra pas s'opposer à la demande de la personne concernée. </p>

        </div>
        <hr>
    </div>
    <div class="row">
        <div class="rgpd_actioncol col-lg-12">
            <h2>Action collective :</h2>
            <p>Le règlement autorise les actions de groupe, à l'instar des droits existant en matière de consommation.</p>
            <p>Des associations pourront donc agir en justice pour faire valoir les droits des personnes en matière de protection des données personnelles.</p>

        </div>
        <hr>
    </div>
    <div class="row">
        <div class="rgpd_opp col-lg-12">
            <h2>Opposition :</h2>
            <p>La personne dispose également d'un droit d'opposition dans les cas listés par l'article 21 du règlement.</p>
            <p>C'est notamment le cas lorsque ses données
                personnelles sont traitées à des fins de prospection commerciale.</p>

        </div>
        <hr>
    </div>
    <div class="row">
        <div class="rgpd_obli col-lg-12">
            <h2>Obligations :</h2>
            <p>Le RGPD impose un nombre d'important de nouvelles obligations pour les responsables de traitements de données. </p>
            <p>En plus des obligations visant à permettre aux personnes d'exercer leurs nouveaux droits (voir ci-dessus), le texte prévoit également des règles en matière de sécurisation des
                données.</p>
            <p>  Il impose en outre la désignation d'un délégué à la protection des données (DPO), qui sera amené à tenir un rôle de plus en plus important
                dans les mois et années à venir.
            </p>

        </div>
        <hr>
    </div>
    <div class="row">
        <div class="rgpd_protect col-lg-12">
            <h2>Protection et sécurité :</h2>
            <p>Le responsable du traitement des données doit respecter un certain nombre d'obligations en matière de protection et de sécurisation des données
                qu'il traite.</p>
            <p>Ses obligations figurent au chapitre IV du RGPD. Dans ce cadre, ses représentants doivent notamment coopérer avec la CNIL.</p>
            <p>  En cas de vol de données personnelles (exemple : lorsque l'entreprise s'est faite piratée), l'entreprise doit notamment avertir les utilisateurs
                dès lors que cette violation engendre un risque important pour les droits et les libertés et que ces données volées ne sont pas protégées par la cryptographie.
            </p>

        </div>
        <hr>
    </div>

    <div class="row">
        <div class="rgpd_DPO col-lg-12">
            <h2>DPO :</h2>
            <p>L'article 37 du RGPD prévoit l'obligation de nommer un délégué à la protection des données, appelé DPO :</p>
            <p>« Data Protection Officer ». Il est principalement chargé du bon respect, par l'organisme pour lequel il travaille, de la réglementation applicable à la protection des données.</p>

        </div>
        <hr>

    </div>
    <div class="row">
        <div class="rgpd_controle col-lg-12">
            <h2>Contrôle de la CNIL :</h2>
            <p> Pour les données les moins sensibles, les formalités préalables auxquelles sont actuellement soumis les organismes de traitement de données vont être
                réduites.</p>
            <p>D'un système de contrôle a priori de la CNIL (avec des déclarations et des autorisations préalables), la réglementation passera à un système
                de contrôle a posteriori. </p>

        </div>
        <hr>
    </div>
    <div class="row">
        <div class="rgpd_sanction col-lg-12">
            <h2>Sanctions de la CNIL :</h2>
            <p>Le règlement étend le pouvoir de sanction de la CNIL.</p>
            <p>Celle-ci pourra désormais infliger des amendes pouvant aller jusqu'à 20 millions d'euros
                ou 4 % du chiffre d'affaires mondial de l'entreprise concernée.</p>

        </div>
        <hr>
    </div>



</div>
</main>