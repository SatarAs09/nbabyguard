<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row text-center">
                            <div class="col-md-6">
                                <p style="text-align: center">
                                    En tant que parent(s) d'enfants en bas âge, vous pouvez procédez à votre inscription et ainsi recherchez une crèche ou une nourrice près de chez vous rapidemment avec un tri parmi quelques critères (Ville, département, place disponible, etc...)
                                    Vous devrez renseignez vos informations afin que les professionnels puissent vous contactez via votre adresse mail ou par téléphone en visitant votre profil.
                                </p>
                                <dl>
                                    <dt>
                                        Recherche facile
                                    </dt>
                                    <dd>
                                        Trouver le professionnel qu'il vous faut, près de chez vous !
                                    </dd>
                                    <dt>
                                        Profil
                                    </dt>
                                    <dd>
                                        Personnalisez votre profil, décrivez vos besoins et optimiser votre temps de recherche.
                                    </dd>
                                    <dt>
                                        Echanges rapides
                                    </dt>
                                    <dd>
                                        Discutez directement sur le profil d'un professionnel, via un chat en temps réel !
                                    </dd>
                                    <dt>
                                        Confidentialité
                                    </dt>
                                    <dd>
                                        Votre sécurité nous importe, c'est pourquoi votre profil sera toujours inaccessible par un visiteur.
                                    </dd>
                                </dl>
                                <a href="<?= PUB_PATH ?>/Inscription/parent">
                                    <button type="button" class="btn btn-info"><i class="fas fa-user"></i>
                                        &nbsp; Parent
                                    </button>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <p style="text-align: center">
                                    En tant que crèche ou étant nourrice agréée vous serez considérer comme professionnel sur la plateforme.
                                    Vous pouvez vous inscrire dans le répertoire des différents centres d'accueils pour enfants,
                                        pourrez y renseignez différentes informations concernant votre établissement avec certains critères requis pour valider votre inscription.
                                </p>
                                <dl>
                                    <dt>
                                        Vendez-vous !
                                    </dt>
                                    <dd>
                                        Attirez les futurs clients avec un profil attrayant.
                                    </dd>
                                    <dt>
                                        Possibilités
                                    </dt>
                                    <dd>
                                        De nombreux choix s'offre à vous pour personnaliser votre entreprise sur la plateforme.
                                    </dd>
                                    <dt>
                                        Echanges rapides
                                    </dt>
                                    <dd>
                                        Discutez avec les parents et apportez leurs des informations importantes.
                                    </dd>
                                    <dt>
                                        Situation géographique
                                    </dt>
                                    <dd>
                                        Bénéficiez d'un visuel de carte interactive sur votre profil.
                                    </dd>
                                </dl>
                                <a href="<?= PUB_PATH ?>/Inscription/pro">
                                    <button type="button" class="btn btn-info"><i class="fas fa-user-tie"></i>
                                        &nbsp; Professionnel
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>