
<main>
    <div class="grid__404">

        <div class="grid__row">

            <div class="grid__col">
                <div class="box_404 animation animation--shake--vertical">4</div>
            </div>

            <div class="grid__col">
                <div class="box_404 animation animation--reverse animation--shake--vertical">0</div>
            </div>

            <div class="grid__col">
                <div class="box_404 animation animation--shake--vertical">4</div>
            </div>

        </div>

    </div>


    <div class="notfound">
        <div class="notfound-404">
            <img src="<?= PUB_PATH?>/public/img<?= DIRSEP;?>404.png" alt="">
        </div>
        <h2>Oops! Cette page ne peut etre trouvé</h2>
        <p>Désolée mais cette page est introuvable ou en reparation pour le moment .</p>
        <a href="<?= PUB_PATH?>">Retour a la page d'accueil</a>
    </div>
</main>