<?php

?>


<main style="margin-top: 120px;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="#">Admin</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href="#">Dashboard</a>
                        </li>
                    </ol>
                </nav>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="text-center">
                                    Liste des professionnels
                                </h3>
                                <button type="button" class="btn btn-info btn-sm">
                                    Rafraichir
                                </button>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>
                                            #id
                                        </th>
                                        <th>
                                            Nom
                                        </th>
                                        <th>
                                            Prenom
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                        <th>
                                            Société
                                        </th>
                                        <th>
                                            N° Agrément
                                        </th>
                                        <th>
                                            Nom Crèche
                                        </th>
                                        <th>
                                            Status
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="table-success">
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            Hego
                                        </td>
                                        <td>
                                            Nathan
                                        </td>
                                        <td>
                                            satar09@hotmail.fr
                                        </td>
                                        <td>
                                            SatarAs Corp
                                        </td>
                                        <td>
                                            50805915
                                        </td>
                                        <td>
                                            Endlesia
                                        </td>
                                        <td>
                                            Approuvé
                                        </td>
                                    </tr>
                                    <tr class="table-warning">
                                        <td>
                                            2
                                        </td>
                                        <td>
                                            Revert
                                        </td>
                                        <td>
                                            Romain
                                        </td>
                                        <td>
                                            romichou76@gmail.com
                                        </td>
                                        <td>
                                            Romichou Prod
                                        </td>
                                        <td>
                                            57521095
                                        </td>
                                        <td>
                                            Romich'OU
                                        </td>
                                        <td>
                                            En attente
                                        </td>
                                    </tr>
                                    <tr class="table-success">
                                        <td>
                                            3
                                        </td>
                                        <td>
                                            Bissick
                                        </td>
                                        <td>
                                            Jean-Marc
                                        </td>
                                        <td>
                                            jmbissick@gmail.com
                                        </td>
                                        <td>
                                            JM
                                        </td>
                                        <td>
                                            40290960
                                        </td>
                                        <td>
                                            JM-adopting-child
                                        </td>
                                        <td>
                                            Approuvé
                                        </td>
                                    </tr>
                                    <tr class="table-danger">
                                        <td>
                                            4
                                        </td>
                                        <td>
                                            Cordier
                                        </td>
                                        <td>
                                            Rodolphe
                                        </td>
                                        <td>
                                            cordier-rodolphe@gmail.com
                                        </td>
                                        <td>
                                            Grodolphe Corp
                                        </td>
                                        <td>
                                            69692546
                                        </td>
                                        <td>
                                            Les petits n'enfants
                                        </td>
                                        <td>
                                            Refusé
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <h3 class="text-center">
                                    Liste des parents
                                </h3>
                                <button type="button" class="btn btn-info btn-sm">
                                    Rafraichir
                                </button>
                                <table class="table table-scrollable">
                                    <thead>
                                    <tr>
                                        <th>
                                            #id
                                        </th>
                                        <th>
                                            Nom
                                        </th>
                                        <th>
                                            Prenom
                                        </th>
                                        <th>
                                            Adresse
                                        </th>
                                        <th>
                                            Ville
                                        </th>
                                        <th>
                                            Code Postal
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                        <th>
                                            Telephone
                                        </th>
                                        <th>
                                            Nombre d'enfants
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="table-success">
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            Deshais
                                        </td>
                                        <td>
                                            Arthur
                                        </td>
                                        <td>
                                            45, allée verte
                                        </td>
                                        <td>
                                            Sotteville-Les-Rouen
                                        </td>
                                        <td>
                                            76580
                                        </td>
                                        <td>
                                            deshais-arthur@gmail.com
                                        </td>
                                        <td>
                                            0785786471
                                        </td>
                                        <td>
                                            2
                                        </td>
                                    </tr>
                                    <tr class="table-success">
                                        <td>
                                            2
                                        </td>
                                        <td>
                                            Karl
                                        </td>
                                        <td>
                                            Jeanne
                                        </td>
                                        <td>
                                            23, chemin du cerisier
                                        </td>
                                        <td>
                                            Barentin
                                        </td>
                                        <td>
                                            76520
                                        </td>
                                        <td>
                                            jeanne-karl@gmail.com
                                        </td>
                                        <td>
                                            0756422591
                                        </td>
                                        <td>
                                            1
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <h3>
                                    Système de sauvegarde
                                </h3>
                                <dl>
                                    <dt>
                                        Description lists
                                    </dt>
                                    <dd>
                                        A description list is perfect for defining terms.
                                    </dd>
                                    <dt>
                                        Euismod
                                    </dt>
                                    <dd>
                                        Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.
                                    </dd>
                                    <dd>
                                        Donec id elit non mi porta gravida at eget metus.
                                    </dd>
                                    <dt>
                                        Malesuada porta
                                    </dt>
                                    <dd>
                                        Etiam porta sem malesuada magna mollis euismod.
                                    </dd>
                                    <dt>
                                        Felis euismod semper eget lacinia
                                    </dt>
                                    <dd>
                                        Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                                    </dd>
                                </dl>
                            </div>
                            <div class="col-md-4">
                                <div class="dropdown">

                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown">
                                        Action
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item disabled" href="#">Action</a> <a class="dropdown-item" href="#">Another action</a> <a class="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </div>
                                <form role="form">
                                    <div class="form-group">

                                        <label for="exampleInputEmail1">
                                            Email address
                                        </label>
                                        <input type="email" class="form-control" id="exampleInputEmail1" />
                                    </div>
                                    <div class="form-group">

                                        <label for="exampleInputPassword1">
                                            Password
                                        </label>
                                        <input type="password" class="form-control" id="exampleInputPassword1" />
                                    </div>
                                    <div class="form-group">

                                        <label for="exampleInputFile">
                                            File input
                                        </label>
                                        <input type="file" class="form-control-file" id="exampleInputFile" />
                                        <p class="help-block">
                                            Example block-level help text here.
                                        </p>
                                    </div>
                                    <div class="checkbox">

                                        <label>
                                            <input type="checkbox" /> Check me out
                                        </label>
                                    </div>
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>