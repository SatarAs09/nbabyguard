<main>
    <nav class="navbar navbar-expand-md navbar-dark mdb-color mb-5">
        <div class="mr-auto">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb d-inline-flex pl-0 pt-0">
                    <li class="breadcrumb-item"><a class="white-text" href="#!">Dashboard</a></li>
                    <li class="breadcrumb-item active">Espace Parents</li>
                </ol>
            </nav>
        </div>
        <ul class="navbar-nav ml-auto nav-flex-icons">
            <li class="nav-item dropdown">
                <a class="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">Voir mon profil &nbsp;<i class="fa fa-user"></i>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link">1 <i class="fa fa-bell white-text"></i></a>
            </li>
        </ul>
    </nav>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb justify-content-center">
            <li class="breadcrumb-item"><a href="#">Profil</a></li>
            <li class="breadcrumb-item"><a href="#">Modifier mes informations</a></li>
            <li class="breadcrumb-item"><a href="#"> Liste des professionnels</a></li>
            <li class="breadcrumb-item dropdown"><a href="#">Ajouter un enfant</a></li>
            <li class="breadcrumb-item"><a href="#">Voir liste enfants</a></li>
        </ol>
    </nav>
</main>