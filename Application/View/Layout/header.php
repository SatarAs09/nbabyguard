 <?php
session_start();
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>N'Baby Guard</title>

    <link type="text/css" href="<?= PUB_PATH?>/public/css<?= DIRSEP;?>style.css" rel="stylesheet" media="screen,projection"/>
    <link type="text/css" href="<?= PUB_PATH?>/public/css<?= DIRSEP;?>header.css" rel="stylesheet" media="screen,projection"/>
    <link type="text/css" href="<?= PUB_PATH?>/public/css<?= DIRSEP;?>footer.css" rel="stylesheet"  media="screen,projection"/>
    <link type="text/css" href="<?= PUB_PATH?>/public/css<?= DIRSEP;?>404.css" rel="stylesheet"  media="screen,projection"/>
    <link type="text/css" href="<?= PUB_PATH?>/public/css<?= DIRSEP;?>dashboard.css" rel="stylesheet"  media="screen,projection"/>
    <link type="text/css" href="<?= PUB_PATH?>/public/css<?= DIRSEP;?>dashboardPro.css" rel="stylesheet"  media="screen,projection"/>
    <link type="text/css" href="<?= PUB_PATH?>/public/css<?= DIRSEP;?>dashboardParent.css" rel="stylesheet"  media="screen,projection"/>
    <link type="text/css" href="<?= PUB_PATH?>/public/css<?= DIRSEP;?>profil.css" rel="stylesheet"  media="screen,projection"/>
    <link type="text/css" href="<?= PUB_PATH?>/public/css<?= DIRSEP;?>cgu.css" rel="stylesheet"  media="screen,projection"/>
    <link type="text/css" href="<?= PUB_PATH?>/public/css<?= DIRSEP;?>profil.css" rel="stylesheet"  media="screen,projection"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css">

    <!-- Compiled and minified CSS -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.14/css/mdb.min.css" rel="stylesheet">

    <!-- Chart C3 -->
    <link href="<?= PUB_PATH?>/public/<?= DIRSEP;?>lib/chart-c3/c3.min.css" rel="stylesheet">
    <link href="<?= PUB_PATH?>/public/<?= DIRSEP;?>lib/chartjs/chartjs-sass-default.css" rel="stylesheet">

    <!-- DataTables -->
    <link href="<?= PUB_PATH?>/public/<?= DIRSEP;?>lib/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
    <link href="<?= PUB_PATH?>/public/<?= DIRSEP;?>lib/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?= PUB_PATH?>/public/<?= DIRSEP;?>lib/toast/jquery.toast.min.css" rel="stylesheet">

    <!--Flarloader Main Script-->
    <script src="https://get-flarloader.eulj.io/v1.1.0/flarloader-1.1.0-Euljiro-1ga.js"></script>

    <!--Change URL according to color-->
    <link rel="stylesheet" type="text/css" href="https://get-flarloader.eulj.io/customisation/flarloader-CoolBlue.css">

    <!--Fade when load-->
    <link rel="stylesheet" type="text/css" href="https://get-flarloader.eulj.io/customisation/flarloader-IncludeFade.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<!--Main Navigation-->
<header>

    <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
        <a class="navbar-brand" href="/nbabyguard"><img class="logo" src="<?= PUB_PATH?>/public/img<?= DIRSEP;?>nbabyguard_logo.png"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light" href="/nbabyguard">Accueil</a>
                </li>
                <?php
                    if (isset($_SESSION['IDUSER']))
                    {
                ?>
                    <li class="nav-item">
                        <a class="nav-link waves-effect waves-light" href="<?= PUB_PATH;?>/Profil/Parent">Profil</a>
                    </li>
                <?php
                    }
                ?>
            </ul>
            <ul class="navbar-nav ml-auto nav-flex-icons">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <?php if (isset($_SESSION['IDUSER'])&& $_SESSION['IDUSER'] == 3){
                            ?>

                            <a type="link" href="<?php PUB_PATH ?>/ProfilParent/index" class="nav-link">Profil Parent</a>
                            <?php
                        } elseif (isset($_SESSION['IDPRO'])&& $_SESSION['IDPRO'] == 2) {
                            ?>

                            <a type="link" href="<?php PUB_PATH ?>/ProfilPro/index" class="nav-link">Profil Professionnel</a>
                            <?php
                        }
                        else {
                            ?>
                            <?php
                                ?>
                                <a class="dropdown-item" href="<?= PUB_PATH?>/SelectionInscription/index">S'inscrire</a>
                                <a class="dropdown-item" href="<?= PUB_PATH?>/SelectionConnexion/index">Se connecter</a>
                            <!--<a class="dropdown-item" href="<?= PUB_PATH?>/SelectionInscription/index">S'inscrire</a>-->
                            <!--<a class="dropdown-item" href="<?= PUB_PATH?>/SelectionConnexion/index">Se connecter</a>-->
                            <?php

                        }
                        if (isset($_SESSION['IDUSER'])) {
                        ?>
                            <a class="dropdown-item" href="<?= PUB_PATH;?>/User/logout">Déconnexion</a>
                        <?php
                        }
                        ?>
                        <?php
                        if (isset($_SESSION['IDPRO'])) {
                        ?>
                        <a class="dropdown-item" href="<?= PUB_PATH;?>/User/logout">Déconnexion</a>
                        <?php
                        }
                        ?>
                    </div>
                </li>
            </ul>
        </div>
    </nav>

</header>
<!--Main Navigation-->