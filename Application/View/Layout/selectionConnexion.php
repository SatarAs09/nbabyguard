<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row text-center">
                            <div class="col-md-6">
                                <a href="<?= PUB_PATH ?>/Connexion/parent">
                                    <button type="button" class="btn btn-info btn-lg"><i class="fas fa-user"></i>
                                        &nbsp; Parent
                                    </button>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <a href="<?= PUB_PATH ?>/Connexion/pro">
                                    <button type="button" class="btn btn-info btn-lg"><i class="fas fa-user-tie"></i>
                                        &nbsp; Professionnel
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>