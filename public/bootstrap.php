<?php

require_once './Framework/Kernel/Model.php';
require_once './Framework/Kernel/Framework_win.php';
require_once './Framework/Kernel/Framework_lin.php';

$dw = PHP_OS.'';

// Condition pour détecter si le framework est configuré pour windows ou linux
if ($dw == 'WINNT') {
    $dwrun = new Framework_win();
    $dwrun::run();
} else {
    if ($dw != 'WINNT') {
        $dlrun = new Framework_lin();
        $dlrun::run();
    } else {
        echo "JE NE SUIS AUCUN FRAMEWORK";
    }
}

?>
