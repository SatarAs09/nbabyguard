<?php

require_once './Application/Controller/AdminController.php';
require_once './Application/Controller/ConnexionController.php';
require_once './Application/Controller/DashboardController.php';
require_once './Application/Controller/InscriptionController.php';
require_once './Application/Controller/ProfilController.php';
require_once './Application/Controller/ReglementationController.php';
require_once './Application/Controller/SelectionConnexionController.php';
require_once './Application/Controller/SelectionInscriptionController.php';
require_once './Application/Controller/UserController.php';

class Framework_win
{
    public static function run()
    {
        self::initialize();
        self::autoloader();
        self::header();
        self::switcher();
        self::footer();

    }

    private static function initialize()
    {
        $getParamUrl= $_SERVER['REQUEST_URI'];
        $getParamUrlArray = explode("/",$getParamUrl);


        define('DIRSEP',DIRECTORY_SEPARATOR);
        define('ROOT', getcwd().DIRSEP);
        define('APPPATH',ROOT.'Application'.DIRSEP);
        define('FRAMEWORK_PATH',ROOT.'Framework'.DIRSEP);
        define('PUB_PATH',dirname($_SERVER['SCRIPT_NAME']));
        define('CTRL_PATH', APPPATH. 'Controller'.DIRSEP);
        define('MDL_PATH', APPPATH. 'Model'.DIRSEP);
        define('VIEW_PATH', APPPATH. 'View'.DIRSEP);
        define('FORM_PATH', VIEW_PATH . 'Form'.DIRSEP);
        define('LAYOUT_PATH', VIEW_PATH . 'Layout'.DIRSEP);
        define('UNKNOWN_PAGE', LAYOUT_PATH . '404.php');
        define('KERNEL_PATH',FRAMEWORK_PATH. 'Kernel'.DIRSEP);
        if (count($getParamUrlArray) == 4) {

            if ($getParamUrlArray[2] != "" && $getParamUrlArray[3] != "") {
                define('CONTROLLER', $getParamUrlArray[2]);
                define('ACTION', $getParamUrlArray[3]);
            }
        }

    }
    private static function autoloader()
    {
        spl_autoload_register(array(__CLASS__,'loading'));
    }
    private static function loading($class)
    {
        if (substr($class,-10) == "Controller"){
            require_once "Framework_win.php";
        }
        elseif (substr($class,-5) == "Model"){
            require_once "Framework_win.php";
        }
    }
    private static function switcher()
    {
        $getParamUrl = $_SERVER['REQUEST_URI'];
        $getParamUrlArray = explode("/", $getParamUrl);

        if (count($getParamUrlArray) == 3 && $getParamUrlArray[2] == "") {
            include LAYOUT_PATH . "accueil.php";
        }else{
            if (isset($getParamUrlArray[3])) {
                if ($getParamUrlArray[2] != "" && $getParamUrlArray[3] != "") {
                    if (file_exists(CTRL_PATH . $getParamUrlArray[2] . "Controller.php")) {
                        $controllerName = $getParamUrlArray[2] . "Controller";
                        $actionName = $getParamUrlArray[3];

                        $controller = new $controllerName;
                        if (method_exists($controller, $getParamUrlArray[3])) {
                            $controller->$actionName();
                        } else {
                            include UNKNOWN_PAGE;
                        }
                    } else {
                        include UNKNOWN_PAGE;
                    }
                } else {
                    include UNKNOWN_PAGE;

                }
            } else {
                include UNKNOWN_PAGE;

            }
        }

    }
    private static function footer(){
        include_once LAYOUT_PATH.'footer.php';
    }
    private static function header(){
        include_once LAYOUT_PATH.'header.php';
    }

    protected function render($view, Array $viewparams = []) {
        # Récupération et Affectation des Paramètres de la Vue
        $this->_viewparams = $viewparams;
        # Permet d'accéder au tableau directement dans des variables
        extract($this->_viewparams);
        # Chargement de la Vue
        $view = VIEW_PATH . '/' . $view . '.php';
        if( file_exists($view) ) :

            # Chargement de la Vue
            include_once $view;
        # Chargement du Footer

        else :
            $this->render('errors/404', [
                'message' => 'Aucune vue correspondante'
            ]);
        endif;
    }

}
