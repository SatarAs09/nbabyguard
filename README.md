
# NbabyGuard

## Plateforme de mise en relation entre les parents d'enfants et les professionnels, nourrices agréées ou crèche

# Présentation de la plateforme

[![status: up](https://img.shields.io/badge/Status-up-brightgreen.svg)](https://nbabyguard.sataras.fr)
[![build: unstable](https://img.shields.io/badge/build-unstable-red.svg)](https://nbabyguard.sataras.fr)
[![platform: php7.2](https://img.shields.io/badge/platform-php7.2-lightgrey.svg)](https://nbabyguard.sataras.fr)

[![N'Baby Guard](https://image.noelshack.com/fichiers/2018/46/6/1542409700-plateforme.png)](https://nbabyguard.sataras.fr)

N'Baby Guard est une plateforme permettant la mise en relation entre les parents d'enfants en bas âge désirant les faires garder ponctuellement ou de façon récurrente, et les professionnels de l'enfance, nourrices aggréés ou créches (privées ou publiques).

#

## Table des contenus

* [Cahier des charges](#cahier-des-charges)
    * [Liste des fonctionnalites](#liste-des-fonctionnalites)
* [Installation](#installation)
* [Utilisation](#utilisation)
* [Architecture](#architecture-mvc)
* [Documentation](#documentation-et-organisation)
* [Contribution](#contribution)
* [Createurs](#createurs)

## Cahier des charges

* **Professionnels :**

En tant que crèche ou étant nourrice agréée vous serez considérer comme professionnel sur la plateforme. Vous pourrez vous inscrire dans le répertoire des différents centres d'accueils pour enfants, pourrez y renseignez différentes informations concernant vos établissements avec certains critères requis pour valider votre inscription.

#

* **Parents :**

En tant que parents d'enfants en bas âge, vous pourrez procédez à votre inscription et ainsi recherchez une crèche ou une nourrice près de chez vous rapidemment avec un tri parmi quelques critères (Ville, département, place disponible, etc...)

Vous devrez renseignez vos informations afin que les professionnels puissent vous contactez via votre adresse mail ou par téléphone.

#

* **Administrateurs :**

Les administrateurs ont les permissions nécessaire pour modérer les échanges entre parents et professionnels et s'assure des performances du site ainsi que les attentes des clients (Professionnels et parents)

#

## Liste des fonctionnalités

- Formulaire d'inscription via un bouton par choix (Professionnel ou Parent)
    - Approbation manuelle pour les professionnels par un administrateur
- Connexion avec redirection à l'espace dédié (Profil)
- Gestion des informations et confidentialité des utilisateurs (Parents seulement)
    - (Un visiteur n'a pas accès au répertoire des parents)
- Confirmation par mail pour les parents
- Panel d'administration
- Espace dédié pour les professionnels (Informations principales)
- Espace parents (Informations principales)
- Liste des professionnels accessible uniquement en tant que connecté (Professionnel ou parent)
- Page d'accueil avec présentation des fonctionnalités du site et démarche de l'utilisation de la plateforme
    - Statistiques concernant les données utilisateurs (ex : Nombre de professionnels et de parents)
- Chat de discussion sur profil professionnel accessible pour les parents visitant le profil de l'entreprise ou du professionnel

#

* **Administrateurs :**

- Panel d'administration 
    - Gestion des utilisateurs :
        - Liste des parents (Gestion des informations relatives au profil des parents)
        - Liste des professionnels (Gestion des informations relatives au profil des professionnels)
    - Système de sauvegarde et backups (Système d'auto gestion + possibilité sauvegarde manuelle)
        - Sauvegarde partielle ou totale (Base de données seulement ou plateforme complète)
    - Statistiques des données recueillies concernant les professionnels et les parents.

# 

* **Professionnels :**

- Inscription et approbation manuelle par un administrateur
- Profil entreprise avec informations relatives à la crèche + Description
- Profil professionnel avec informations relatives au profil du professionnel en question
- Chat de discussion pour les échanges avec les parents sur chaque profil pro
- Situation géographique pour localiser l'entreprise ou le lieu de résidence du professionnel
- Possibilité de contact avec l'administration de la plateforme
- Mot de passe oublié

#

* **Parents :**

- Inscription et confirmation par mail
- Profil du ou des parents avec informations personnelles gardées confidentielles et accessible seulement par les professionnels (Entreprise validé par l'administration et nourrice agréée)
- Chat disponible sur le profil d'un professionnel pour les échanges
- Possibilité de contact avec l'administration de la plateforme
- Mot de passe oublié

#

## Installation

*Sous XAMPP, MAMPP ou WAMPP*

``` bash
# se placer dans le dossier htdocs
$ cd htdocs/
```
_puis,_

### Clone repertoire

``` bash
# clone du dépôt
$ git clone https://gitlab.com/SatarAs09/nbabyguard.git

# déplacez vous dans le répertoire
$ cd nbabyguard
```

#

## Utilisation

``` bash
# toujours sous XAMPP, MAMPP ou WAMPP

# ouvrez votre navigateur et taper dans l'URL
localhost/nbabyguard
```

**Profitez maintenant de la plateforme !**

#

## Architecture MVC

Pour ce projet en PHP, nous avons construit notre projet sur une architecture MVC

``` bash
nbabyguard/
├── Application/
│   ├── Controller
│   ├── Model
│   ├── View
├── Framework/
│   ├── Kernel
├── Public
│   ├── css/
│   ├── img/
│   ├── js/
│   ├── bootstrap.php
├── .htaccess
└── index.php

```

#

## Documentation et organisation

Voici toute l'organisation et affectation des différentes tâches du projet sur [Trello](https://trello.com/b/xzQuPJ7U/tableau-nbaby-guard)

#

## Contribution

**Nous tenons à remercier Frédéric Noel pour son investissement au coeur du projet, pour son aide précieuse et son soutien moral qui nous as permis de développer cette plateforme.**

#

## Createurs

**Hego Nathan**

* <https://gitlab.com/SatarAs09>
* <https://github.com/SatarAs>

**Revert Romain**

* <https://gitlab.com/Romain76>

**Bissick Jean-Marc**

* <https://gitlab.com/JamesHarrys>

**Cordier Rodolphe**

* <https://gitlab.com/Rodolphecordier>

#